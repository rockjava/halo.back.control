这是一个后台管理系统
采用spring boot，集成freemarker，shiro，spring cache，spring data jpa
前端采用bootstrap，dataTables 等等一些跟bootstrap集成的插件

记住要编译过之后在执行下面的操作

1. 这个项目是maven项目，需要本地安装maven才行
2. 数据库脚本在{project}/src/main/resources/init-data.sql
3. 然后在{project}/src/main/resources/application.yml文件中修改数据库配置

```
spring:
  datasource:
    type: com.alibaba.druid.pool.DruidDataSource
    driver-class-name: com.mysql.jdbc.Driver
    url: jdbc:mysql://localhost:3306/halo
    username: root
    password: 123456
```

4. 部署好之后运行{project}/src/main/java/com/control/back/halo/SampleSpringApplication.java




![登录](http://git.oschina.net/uploads/images/2016/1228/164411_92da7c23_20816.png "登录")
![角色管理](http://git.oschina.net/uploads/images/2016/1228/165013_1506ddfd_20816.png "角色管理")
![角色权限](http://git.oschina.net/uploads/images/2016/1228/165221_fc7ffeab_20816.png "角色权限")